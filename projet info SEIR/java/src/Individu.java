public class Individu {
    private MersenneTwister mt = new MersenneTwister();
    char statut;  // peut être S (Susceptible), E (Exposed), I (Infected), R (Recovered)
    int temps=0; // temps ecroulé dans un état x
    private double dE = negExp(3);
    private double dI = negExp(7);
    private double dR = negExp(365);
    double pE; //probabilité d'ête infecté apres exposition à un agent infectieux voisin
    boolean estExpose(Individu voisin){
        return voisin.statut == 'I';
    }
    double negExp(double inMean){
        return -inMean * Math.log(1 - mt.nextDouble());
    }
    void devientExpose(Individu[][] tab, int posX, int posY) {
        int nbVoisinInfectieux=0;
        int[] offsetsX = {-1, 0, -1, -1, 0, 1, 1, 1};
        int[] offsetsY = {-1, -1, 0, 1, 1, 1, 0, -1};
    
        for (int i = 0; i < offsetsX.length; i++) {
            int newX = posX + offsetsX[i];
            int newY = posY + offsetsY[i];
            if (isValidPosition(newX, newY, tab.length, tab[0].length) && estVoisinValid(tab, newX, newY)){
                if ( this.estExpose(tab[newX][newY])) {
                    nbVoisinInfectieux++;
                }
            } 
        }
        this.pE = 1 - Math.exp(-0.5*nbVoisinInfectieux);
        double random = mt.nextDouble();
        if (this.pE>random){
            this.statut='E';
            this.temps = 0;
        }
        
    }
    
    private boolean isValidPosition(int x, int y, int maxX, int maxY) {
        return x >= 0 && x < maxX && y >= 0 && y < maxY;
    }
    private boolean estVoisinValid(Individu[][] tab, int posX, int posY){
        return tab[posX][posY]!=null;
    }
    void devientInfectieux(){
        if (this.temps > this.dE){
            this.statut = 'I';
            this.temps = 0;
        }
    }

    void recupere(){
        if (this.temps > dI){
            this.statut = 'R';
            this.temps = 0;
        }
    }

    void devientSusceptible(){
        if (this.temps > this.dR){
            this.statut = 'S'; 
            this.temps = 0;
        }
    }
}