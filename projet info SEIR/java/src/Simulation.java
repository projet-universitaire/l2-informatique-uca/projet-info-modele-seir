import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Simulation {
    static MersenneTwister mt;
    int numSimulations = 100;
    long[] seeds;
    private Simulation() {
        mt = new MersenneTwister();
        seeds = new long[numSimulations];

        for (int i = 0; i < numSimulations; i++) {
            seeds[i] = mt.nextLong(); // Génère un statut de départ unique pour chaque simulation
        }
    }
    private void genereTab(Individu[][] tab, int simulationIndex) {
    mt.setSeed(seeds[simulationIndex]);

    int totalCells = tab.length * tab[0].length;
    int totalIndividuals = 20000;

    // Crée une liste de toutes les positions possibles
    List<Integer> allPositions = new ArrayList<>();
    for (int i = 0; i < totalCells; i++) {
        allPositions.add(i);
    }

    // Mélange la liste des positions
    Collections.shuffle(allPositions, mt);

    // Place les individus S aux positions aléatoires
    for (int i = 0; i < totalIndividuals; i++) {
        int position = allPositions.get(i);
        int x = position / tab[0].length;
        int y = position % tab[0].length;

        Individu individu;
        if (i < 19980) {
            individu = new Individu();
            individu.statut = 'S';
        } else {
            individu = new Individu();
            individu.statut = 'I';
        }

        tab[x][y] = individu;
    }
}
    
    
    /* 
    // Affiche un tableau à deux dimensions
    private static void afficherTableau(Individu[][] tableau) {
        for (int i = 0; i < tableau.length; i++) {
            for (int j = 0; j < tableau[i].length; j++) {
                System.out.print(tableau[i][j].statut + "\t");
            }
            System.out.println();
        }
    }*/
    void incrementerTemps(Individu[][] tab){
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                if (tab[i][j] != null)
                    tab[i][j].temps++;
            }
        }
    }
    private void createCSV(int simulationIndex) {
        String folderPath = "csv";
        String fileName = "simulation_" + simulationIndex + ".csv";

        Path filePath = Paths.get(folderPath, fileName);

        try {
            Files.createDirectories(filePath.getParent());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileWriter writer = new FileWriter(filePath.toString())) {
            writer.write("jour,");
            writer.write("S,");
            writer.write("I,");
            writer.write("E,");
            writer.write("R,");
            writer.write("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void writeToCSV(int temps, double[] tab, int simulationIndex) {
        String folderPath = "csv";
        String fileName = "simulation_" + simulationIndex + ".csv";

        Path filePath = Paths.get(folderPath, fileName);
        
        try (FileWriter writer = new FileWriter(filePath.toString(), true)) {
            writer.write(temps+",");
            for (int i = 0; i < tab.length; i++) {
                writer.write(tab[i] + ",");
            }
            writer.write("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    double[] calcul(Individu[][] tab){
        double[] tabM = {0,0,0,0}; // S | I | E | R
        for(int i =0; i < tab.length; i++ ){
            for(int j = 0; j < tab[i].length; j++){ 
                if (tab[i][j]!=null){
                    switch (tab[i][j].statut) {
                        case 'S':
                            tabM[0]++;
                            break;
                        case 'I':
                            tabM[1]++;
                            break;
                        case 'E':
                            tabM[2]++;
                            break;
                        case 'R':
                            tabM[3]++;
                            break;
                    }
                }
            }
        }
        return tabM;
    }
    private void permuterCases(Individu[][] tab) {
        List<Individu> individus = new ArrayList<>();
    
        // Collecte tous les individus non-null du tableau dans une liste
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                if (tab[i][j] != null) {
                    individus.add(tab[i][j]);
                }
            }
        }
    
        // Mélange la liste d'individus
        Collections.shuffle(individus, mt);
    
        // Réassigne les individus au tableau de manière aléatoire
        int index = 0;
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                if (index < individus.size()) {
                    tab[i][j] = individus.get(index);
                    index++;
                } else {
                    tab[i][j] = null; // Les cases restantes sont nulles
                }
            }
        }
    }
    
    /*public static void main(String[] args) {
        Simulation simulation = new Simulation();
        for (int i = 1; i <= simulation.numSimulations; i++) {
            Individu[][] tab = new Individu[300][300];
            simulation.genereTab(tab, i);
            simulation.createCSV(i);
            for (int j = 0; j <= 730; j++){
                int ligne = mt.nextInt(tab.length);
                int col = mt.nextInt(tab[0].length);
                while (tab[ligne][col]==null){
                    ligne = mt.nextInt(tab.length);
                    col = mt.nextInt(tab[0].length);
                }
                switch (tab[ligne][col].statut){
                    case 'I':
                        tab[ligne][col].recupere();
                        break;
                    case 'E':
                        tab[ligne][col].devientInfectieux();
                        break;
                    case 'R':
                        tab[ligne][col].devientSusceptible();
                        break;  
                    case 'S':
                        tab[ligne][col].devientExpose(tab, ligne, col);
                        break;
                }
                simulation.incrementerTemps(tab);
                double[] tabM = simulation.calcul(tab);
                // Écriture des données dans un fichier CSV après chaque itération
                simulation.writeToCSV(j,tabM,i);
                simulation.permuterCases(tab);
            }
        } 
    }*/

    public class Position {
        int ligne;
        int colonne;
    
        public Position(int ligne, int colonne) {
            this.ligne = ligne;
            this.colonne = colonne;
        }
    }
    
    
    private Position trouverPosition(Individu[][] tab, Individu individu) {
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                if (tab[i][j] != null && tab[i][j] == individu) {
                    return new Position(i, j);
                }
            }
        }
        // Retourne null si l'individu n'est pas trouvé
        return null;
    }
    public static void main(String[] args) {
        Simulation simulation = new Simulation();
        for (int i = 1; i <= simulation.numSimulations + 1; i++) {
            Individu[][] tab = new Individu[300][300];
            simulation.genereTab(tab, i);
            simulation.createCSV(i);
    
            for (int j = 0; j <= 730; j++) {
                simulation.incrementerTemps(tab);
                double[] tabM = simulation.calcul(tab);
    
                // Parcourir de manière aléatoire chaque individu pour effectuer des actions
                List<Individu> individus = new ArrayList<>();
                for (int x = 0; x < tab.length; x++) {
                    for (int y = 0; y < tab[x].length; y++) {
                        if (tab[x][y] != null) {
                            individus.add(tab[x][y]);
                        }
                    }
                }
                Collections.shuffle(individus, mt);
    
                for (Individu individu : individus) {
                    //int ligne = mt.nextInt(tab.length);
                    //int col = mt.nextInt(tab[0].length);
                    Position positionIndividu = simulation.trouverPosition(tab, individu);
                    // Effectuer des actions sur l'individu en position (ligne, col)
                    switch (individu.statut) {
                        case 'I':
                            individu.recupere();
                            break;
                        case 'E':
                            individu.devientInfectieux();
                            break;
                        case 'R':
                            individu.devientSusceptible();
                            break;
                        case 'S':
                            individu.devientExpose(tab, positionIndividu.ligne, positionIndividu.colonne);
                            break;
                    }
                }
                
                // Écriture des données dans un fichier CSV après chaque itération
                simulation.writeToCSV(j, tabM, i);
                simulation.permuterCases(tab);
            }
    
            
        }
    }
}