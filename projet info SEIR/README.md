# TP4_projet_informatique

## Membre du groupe:
- AAKRI Mohammed
- ATES Ali
- TOGBOSSI Seho Fred

## Langage uilisé: Java

## Structure du depot :
- le dossier java contient : 
    - src : dossier contenant les codes sources
    - file : dossier contenant les fichiers générés
    
- le dossier csv contient les données générées par le code source.

- jupyter : contient les fichiers jupyter des courbes 

- autres :  
    - rapport
    - lien des sources utilisés